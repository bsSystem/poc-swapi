(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<br><br>\n<div class=\"container\">\n    <div class=\"section\">\n        <div class=\"row\">\n            <div class=\"col s12 m12 center\">\n                <div class=\"icon-block\">\n                    <img src=\"./assets/img/darth-vader.png\" alt=\"\">\n                    <h5 class=\"center\">Welcome to the Swapi app!</h5>\n                    <p class=\"light\">See the list of movies \"Star Wars\", and your details.</p>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<br><br>\n<app-swapi-list></app-swapi-list>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/swapi-detail/swapi-detail.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/swapi-detail/swapi-detail.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<div class=\"container\">\n  <div class=\"section\">\n    <div class=\"row\">\n      <div class=\"col s12 m12 center\">\n        <div class=\"icon-block\">\n          <img src=\"./assets/img/darth-vader.png\" alt=\"\">\n          <h5 class=\"center\">In the details...</h5>\n          <p class=\"light\">See below for all information related to film.</p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col s12 left\">\n      <button id=\"back-button\" class=\"btn-large waves-effect waves-light green lighten-1\" (click)=\"back()\">\n        <i class=\"material-icons left\">arrow_back</i> Back\n      </button>\n    </div>\n  </div>\n  <div class=\"col s12\">\n\n    <div class=\"card hoverable\">\n\n      <div *ngIf=\"!swapi\" class=\"preloader-wrapper small active loaders\">\n        <div class=\"spinner-layer spinner-green-only\">\n          <div class=\"circle-clipper left\">\n            <div class=\"circle\"></div>\n          </div>\n          <div class=\"gap-patch\">\n            <div class=\"circle\"></div>\n          </div>\n          <div class=\"circle-clipper right\">\n            <div class=\"circle\"></div>\n          </div>\n        </div>\n      </div>\n\n      <div class=\"card-content\" [ngClass]=\"{'loader-card': !swapi}\">\n        <div class=\"row\">\n          <div class=\"col s12\">\n            <h3>{{swapi?.title}}</h3>\n            <ul>\n              <li>\n                <p><b>Director:</b> {{swapi?.director}}</p>\n              </li>\n              <li>\n                <p><b>Producer:</b> {{swapi?.producer}}</p>\n              </li>\n              <li>\n                <p><b>Date Release:</b> {{swapi?.release_date}}</p>\n              </li>\n            </ul>\n\n            <p>{{swapi?.opening_crawl}}</p>\n          </div>\n        </div>\n\n        <div class=\"row\">\n          <div class=\"col s4\">\n            <ul class=\"collection with-header\">\n              <li class=\"collection-header\">\n                <h6>Characters</h6>\n              </li>\n            </ul>\n            <div class=\"col s2\" *ngFor=\"let pers of swapi?.characters\">\n              <a href=\"{{pers}}\" title=\"{{pers}}\"><img src=\"./assets/img/darth-vader.png\" alt=\"{{pers}}\"></a>\n            </div>\n          </div>\n\n          <div class=\"col s4\">\n            <ul class=\"collection with-header\">\n              <li class=\"collection-header\">\n                <h6>Planets</h6>\n              </li>\n            </ul>\n            <div class=\"col s2\" *ngFor=\"let planet of swapi?.planets\">\n              <a href=\"{{planet}}\" title=\"{{planet}}\"><img src=\"./assets/img/sistema-solar.png\" alt=\"{{planet}}\"></a>\n            </div>\n          </div>\n\n          <div class=\"col s4\">\n              <ul class=\"collection with-header\">\n                <li class=\"collection-header\">\n                  <h6>Starships</h6>\n                </li>\n              </ul>\n              <div class=\"col s2\" *ngFor=\"let starship of swapi?.starships\">\n                <a href=\"{{starship}}\" title=\"{{starship}}\"><img src=\"./assets/img/foguete-espacial.png\" alt=\"{{starship}}\"></a>\n              </div>\n            </div>\n          \n        </div>\n\n        <div class=\"row\">\n            <div class=\"col s6\">\n              <ul class=\"collection with-header\">\n                <li class=\"collection-header\">\n                  <h6>Vehicles</h6>\n                </li>\n              </ul>\n              <div class=\"col s2\" *ngFor=\"let vehicle of swapi?.vehicles\">\n                <a href=\"{{vehicle}}\" title=\"{{vehicle}}\"><img src=\"./assets/img/transportes.png\" alt=\"{{vehicle}}\"></a>\n              </div>\n            </div>\n  \n            <div class=\"col s6\">\n              <ul class=\"collection with-header\">\n                <li class=\"collection-header\">\n                  <h6>Species</h6>\n                </li>\n              </ul>\n              <div class=\"col s2\" *ngFor=\"let specie of swapi?.species\">\n                <a href=\"{{specie}}\" title=\"{{specie}}\"><img src=\"./assets/img/especie.png\" alt=\"{{specie}}\"></a>\n              </div>\n            </div>\n  \n          </div>\n\n\n\n      </div>\n    </div>\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/dashboard/swapi-list/swapi-list.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/dashboard/swapi-list/swapi-list.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div *ngIf=\"!films\" class=\"preloader-wrapper small active loaders\">\n            <div class=\"spinner-layer spinner-green-only\">\n                <div class=\"circle-clipper left\">\n                    <div class=\"circle\"></div>\n                </div>\n                <div class=\"gap-patch\">\n                    <div class=\"circle\"></div>\n                </div>\n                <div class=\"circle-clipper right\">\n                    <div class=\"circle\"></div>\n                </div>\n            </div>\n        </div>\n        <div class=\"col s12\" *ngFor=\"let film of films?.results; let i = index\">\n            <div class=\"card horizontal\">\n                <div class=\"card-stacked\">\n                    <div class=\"card-content\">\n                        <div class=\"col s7\">\n                            <h5 class=\"blue-text text-darken-2\"><i class=\"material-icons\">movie</i> {{film?.title}}</h5>\n                            <ul>\n                                <li>\n                                    <p><b>Director:</b> {{film?.director}}</p>\n                                </li>\n                                <li>\n                                    <p><b>Date Release:</b> {{film?.release_date}}</p>\n                                </li>\n                            </ul>\n                        </div>\n                        <div class=\"col s5\">\n\n                            <ul class=\"collection with-header\">\n                                <li class=\"collection-header\">\n                                    <h6>Characters</h6>\n                                </li>\n                            </ul>\n                            <div class=\"col s2\" *ngFor=\"let pers of film?.characters\">\n                                <a href=\"{{pers}}\" title=\"{{pers}}\"><img src=\"./assets/img/darth-vader.png\"\n                                        alt=\"{{pers}}\"></a>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"card-action\">\n                        <a class=\"secondary-content\" [routerLink]=\"['/detail', i+1]\">More Details about</a>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/header/header.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/header/header.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"green lighten-1\" role=\"navigation\">\n    <div class=\"nav-wrapper\">\n        <ul class=\"right\">\n            <li><a class=\"waves-effect waves-light btn green lighten-2\" (click)=\"logout()\">Log out</a></li>\n        </ul>\n    </div>\n</nav>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html":
/*!**********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login/login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"section no-pad-bot\" id=\"index-banner\">\n    <div class=\"container\">\n        <br><br>\n        <h1 class=\"header center active\">POC Swapi</h1>\n        <div class=\"row center\">\n            <h5 class=\"header col s12 light\">Dear appraiser, follow the instructions in the READ.me file for content access.</h5>\n        </div>\n        <br><br>\n        <div class=\"row center\">\n            <form [formGroup]=\"form\" class=\"col s12\">\n                <div class=\"row center\">\n                    <div class=\"container\">\n                        <div class=\"input-field col s6\">\n                            <i class=\"material-icons prefix\">account_circle</i>\n                            <input formControlName=\"account\" id=\"icon_account\" type=\"text\" class=\"validate\">\n                            <label class=\"active\" for=\"name\">Account</label>\n                        </div>\n                        <div class=\"input-field col s6\">\n                            <i class=\"material-icons prefix\">lock</i>\n                            <input formControlName=\"password\" id=\"icon_password\" type=\"password\" class=\"validate\">\n                            <label class=\"active\" for=\"name\">Password</label>\n                        </div>\n                    </div>\n                </div>\n            </form>\n        </div>\n        <div class=\"row center\">\n            <button href=\"\" id=\"login-button\" class=\"btn-large waves-effect waves-light green lighten-1\" (click)=\"login()\">login</button>\n        </div>\n        <br><br>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth-guard */ "./src/app/auth-guard.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _dashboard_swapi_detail_swapi_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard/swapi-detail/swapi-detail.component */ "./src/app/dashboard/swapi-detail/swapi-detail.component.ts");







const routes = [
    { path: '', component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] },
    { path: 'detail/:id', component: _dashboard_swapi_detail_swapi_detail_component__WEBPACK_IMPORTED_MODULE_6__["SwapiDetailComponent"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'POC Swapi';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth-guard */ "./src/app/auth-guard.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var ngx_materialize__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-materialize */ "./node_modules/ngx-materialize/fesm2015/ngx-materialize.js");
/* harmony import */ var _dashboard_service_dashboard_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./dashboard/service/dashboard.service */ "./src/app/dashboard/service/dashboard.service.ts");
/* harmony import */ var _dashboard_service_dashboard_rest_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./dashboard/service/dashboard-rest.service */ "./src/app/dashboard/service/dashboard-rest.service.ts");
/* harmony import */ var _login_login_form_login_form__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./login/login-form/login-form */ "./src/app/login/login-form/login-form.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./header/header.module */ "./src/app/header/header.module.ts");
/* harmony import */ var _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./dashboard/dashboard.module */ "./src/app/dashboard/dashboard.module.ts");
/* harmony import */ var _dashboard_swapi_list_swapi_list_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./dashboard/swapi-list/swapi-list.module */ "./src/app/dashboard/swapi-list/swapi-list.module.ts");
/* harmony import */ var _dashboard_swapi_detail_swapi_detail_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./dashboard/swapi-detail/swapi-detail.module */ "./src/app/dashboard/swapi-detail/swapi-detail.module.ts");



















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_14__["LoginComponent"]
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            ngx_materialize__WEBPACK_IMPORTED_MODULE_10__["MzInputModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            ngx_materialize__WEBPACK_IMPORTED_MODULE_10__["MzValidationModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
            _header_header_module__WEBPACK_IMPORTED_MODULE_15__["HeaderModule"],
            _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_16__["DashboardModule"],
            _dashboard_swapi_list_swapi_list_module__WEBPACK_IMPORTED_MODULE_17__["SwapiListModule"],
            _dashboard_swapi_detail_swapi_detail_module__WEBPACK_IMPORTED_MODULE_18__["SwapiDetailModule"]
        ],
        providers: [
            _login_login_form_login_form__WEBPACK_IMPORTED_MODULE_13__["LoginForm"],
            _auth_guard__WEBPACK_IMPORTED_MODULE_7__["AuthGuard"],
            _dashboard_service_dashboard_service__WEBPACK_IMPORTED_MODULE_11__["DashboardService"],
            _dashboard_service_dashboard_rest_service__WEBPACK_IMPORTED_MODULE_12__["DashboardRestService"]
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth-guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth-guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let AuthGuard = class AuthGuard {
    constructor(router) {
        this.router = router;
        this.isAuthenticated = false;
    }
    canActivate(route, state) {
        if (this.isAuthenticated) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
    validLogin() {
        this.isAuthenticated = true;
    }
    logout() {
        this.isAuthenticated = false;
    }
};
AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], AuthGuard);



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DashboardComponent = class DashboardComponent {
    constructor() { }
    ngOnInit() {
    }
};
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: __webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/dashboard.component.html"),
        styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], DashboardComponent);



/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../header/header.module */ "./src/app/header/header.module.ts");
/* harmony import */ var _swapi_list_swapi_list_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./swapi-list/swapi-list.module */ "./src/app/dashboard/swapi-list/swapi-list.module.ts");
/* harmony import */ var _swapi_detail_swapi_detail_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./swapi-detail/swapi-detail.module */ "./src/app/dashboard/swapi-detail/swapi-detail.module.ts");









let DashboardModule = class DashboardModule {
};
DashboardModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _header_header_module__WEBPACK_IMPORTED_MODULE_6__["HeaderModule"],
            _swapi_list_swapi_list_module__WEBPACK_IMPORTED_MODULE_7__["SwapiListModule"],
            _swapi_detail_swapi_detail_module__WEBPACK_IMPORTED_MODULE_8__["SwapiDetailModule"]
        ],
        exports: [_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        entryComponents: [_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"]]
    })
], DashboardModule);



/***/ }),

/***/ "./src/app/dashboard/service/dashboard-rest.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/dashboard/service/dashboard-rest.service.ts ***!
  \*************************************************************/
/*! exports provided: DashboardRestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRestService", function() { return DashboardRestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");





let DashboardRestService = class DashboardRestService {
    constructor(http) {
        this.http = http;
    }
    getAllFilms() {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])({})
            .pipe(() => this.http.get(`${src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__["environment"].END_POINT}`));
    }
    getFilmById(id) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])({})
            .pipe(() => this.http.get(`${src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__["environment"].END_POINT}${id}/`));
    }
};
DashboardRestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], DashboardRestService);



/***/ }),

/***/ "./src/app/dashboard/service/dashboard.service.ts":
/*!********************************************************!*\
  !*** ./src/app/dashboard/service/dashboard.service.ts ***!
  \********************************************************/
/*! exports provided: DashboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function() { return DashboardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _dashboard_rest_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard-rest.service */ "./src/app/dashboard/service/dashboard-rest.service.ts");





let DashboardService = class DashboardService {
    constructor(dashboardRestService) {
        this.dashboardRestService = dashboardRestService;
    }
    getAllFilms() {
        return this.dashboardRestService.getAllFilms()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(error => {
            console.error({ error });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])();
        }));
    }
    getFilmById(id) {
        return this.dashboardRestService.getFilmById(id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(error => {
            console.error({ error });
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])();
        }));
    }
};
DashboardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_dashboard_rest_service__WEBPACK_IMPORTED_MODULE_4__["DashboardRestService"]])
], DashboardService);



/***/ }),

/***/ "./src/app/dashboard/swapi-detail/swapi-detail.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/dashboard/swapi-detail/swapi-detail.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loaders {\n    display: flex !important;\n    align-items: start;\n    position: absolute!important;\n    z-index: 100;\n    left:0; right:0;\n    top:0; bottom:0;\n    margin:auto;\n}\n\n.loader-card {\n    -webkit-filter: blur(2px);\n            filter: blur(2px);\n}\n\nul:not(.browser-default)>li {\n   list-style-type: circle;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3N3YXBpLWRldGFpbC9zd2FwaS1kZXRhaWwuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsNEJBQTRCO0lBQzVCLFlBQVk7SUFDWixNQUFNLEVBQUUsT0FBTztJQUNmLEtBQUssRUFBRSxRQUFRO0lBQ2YsV0FBVztBQUNmOztBQUVBO0lBQ0kseUJBQWlCO1lBQWpCLGlCQUFpQjtBQUNyQjs7QUFFQTtHQUNHLHVCQUF1QjtBQUMxQiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9zd2FwaS1kZXRhaWwvc3dhcGktZGV0YWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGVycyB7XG4gICAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICAgIGFsaWduLWl0ZW1zOiBzdGFydDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGUhaW1wb3J0YW50O1xuICAgIHotaW5kZXg6IDEwMDtcbiAgICBsZWZ0OjA7IHJpZ2h0OjA7XG4gICAgdG9wOjA7IGJvdHRvbTowO1xuICAgIG1hcmdpbjphdXRvO1xufVxuXG4ubG9hZGVyLWNhcmQge1xuICAgIGZpbHRlcjogYmx1cigycHgpO1xufVxuXG51bDpub3QoLmJyb3dzZXItZGVmYXVsdCk+bGkge1xuICAgbGlzdC1zdHlsZS10eXBlOiBjaXJjbGU7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/swapi-detail/swapi-detail.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/dashboard/swapi-detail/swapi-detail.component.ts ***!
  \******************************************************************/
/*! exports provided: SwapiDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwapiDetailComponent", function() { return SwapiDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _service_dashboard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/dashboard.service */ "./src/app/dashboard/service/dashboard.service.ts");





let SwapiDetailComponent = class SwapiDetailComponent {
    constructor(location, activatedRoute, dashboardService) {
        this.location = location;
        this.activatedRoute = activatedRoute;
        this.dashboardService = dashboardService;
    }
    ngOnInit() {
        this.getFilmById();
    }
    getFilmById() {
        this.idFilm = this.activatedRoute.snapshot.paramMap.get('id');
        this.dashboardService.getFilmById(this.idFilm)
            .subscribe(res => {
            this.swapi = res;
        });
    }
    back() {
        this.location.back();
    }
};
SwapiDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-swapi-detail',
        template: __webpack_require__(/*! raw-loader!./swapi-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/swapi-detail/swapi-detail.component.html"),
        styles: [__webpack_require__(/*! ./swapi-detail.component.css */ "./src/app/dashboard/swapi-detail/swapi-detail.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _service_dashboard_service__WEBPACK_IMPORTED_MODULE_4__["DashboardService"]])
], SwapiDetailComponent);



/***/ }),

/***/ "./src/app/dashboard/swapi-detail/swapi-detail.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/dashboard/swapi-detail/swapi-detail.module.ts ***!
  \***************************************************************/
/*! exports provided: SwapiDetailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwapiDetailModule", function() { return SwapiDetailModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _swapi_detail_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./swapi-detail.component */ "./src/app/dashboard/swapi-detail/swapi-detail.component.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../header/header.module */ "./src/app/header/header.module.ts");







let SwapiDetailModule = class SwapiDetailModule {
};
SwapiDetailModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_swapi_detail_component__WEBPACK_IMPORTED_MODULE_5__["SwapiDetailComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _header_header_module__WEBPACK_IMPORTED_MODULE_6__["HeaderModule"]
        ],
        exports: [_swapi_detail_component__WEBPACK_IMPORTED_MODULE_5__["SwapiDetailComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        entryComponents: [_swapi_detail_component__WEBPACK_IMPORTED_MODULE_5__["SwapiDetailComponent"]]
    })
], SwapiDetailModule);



/***/ }),

/***/ "./src/app/dashboard/swapi-list/swapi-list.component.css":
/*!***************************************************************!*\
  !*** ./src/app/dashboard/swapi-list/swapi-list.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\n  cursor: pointer;\n}\n\n.left {\n  text-align: left;\n}\n\n.loaders {\n  display: flex!important;\n  align-items: start;\n  position: absolute!important;\n  z-index: 100;\n  left:0; right:0;\n  top:0; bottom:0;\n  margin:auto;\n}\n\n.loader-card {\n  -webkit-filter: blur(2px);\n          filter: blur(2px);\n}\n\nul:not(.browser-default)>li {\n list-style-type: circle;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL3N3YXBpLWxpc3Qvc3dhcGktbGlzdC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsNEJBQTRCO0VBQzVCLFlBQVk7RUFDWixNQUFNLEVBQUUsT0FBTztFQUNmLEtBQUssRUFBRSxRQUFRO0VBQ2YsV0FBVztBQUNiOztBQUVBO0VBQ0UseUJBQWlCO1VBQWpCLGlCQUFpQjtBQUNuQjs7QUFFQTtDQUNDLHVCQUF1QjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9zd2FwaS1saXN0L3N3YXBpLWxpc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImEge1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5sZWZ0IHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuLmxvYWRlcnMge1xuICBkaXNwbGF5OiBmbGV4IWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IHN0YXJ0O1xuICBwb3NpdGlvbjogYWJzb2x1dGUhaW1wb3J0YW50O1xuICB6LWluZGV4OiAxMDA7XG4gIGxlZnQ6MDsgcmlnaHQ6MDtcbiAgdG9wOjA7IGJvdHRvbTowO1xuICBtYXJnaW46YXV0bztcbn1cblxuLmxvYWRlci1jYXJkIHtcbiAgZmlsdGVyOiBibHVyKDJweCk7XG59XG5cbnVsOm5vdCguYnJvd3Nlci1kZWZhdWx0KT5saSB7XG4gbGlzdC1zdHlsZS10eXBlOiBjaXJjbGU7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/swapi-list/swapi-list.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/dashboard/swapi-list/swapi-list.component.ts ***!
  \**************************************************************/
/*! exports provided: SwapiListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwapiListComponent", function() { return SwapiListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _service_dashboard_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../service/dashboard.service */ "./src/app/dashboard/service/dashboard.service.ts");



let SwapiListComponent = class SwapiListComponent {
    constructor(dashboardService) {
        this.dashboardService = dashboardService;
    }
    ngOnInit() {
        this.loadData();
    }
    loadData() {
        this.dashboardService.getAllFilms()
            .subscribe(res => {
            this.films = res;
        });
    }
};
SwapiListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-swapi-list',
        template: __webpack_require__(/*! raw-loader!./swapi-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/dashboard/swapi-list/swapi-list.component.html"),
        styles: [__webpack_require__(/*! ./swapi-list.component.css */ "./src/app/dashboard/swapi-list/swapi-list.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_dashboard_service__WEBPACK_IMPORTED_MODULE_2__["DashboardService"]])
], SwapiListComponent);



/***/ }),

/***/ "./src/app/dashboard/swapi-list/swapi-list.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/dashboard/swapi-list/swapi-list.module.ts ***!
  \***********************************************************/
/*! exports provided: SwapiListModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwapiListModule", function() { return SwapiListModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _swapi_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./swapi-list.component */ "./src/app/dashboard/swapi-list/swapi-list.component.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../header/header.module */ "./src/app/header/header.module.ts");







let SwapiListModule = class SwapiListModule {
};
SwapiListModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_swapi_list_component__WEBPACK_IMPORTED_MODULE_5__["SwapiListComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _header_header_module__WEBPACK_IMPORTED_MODULE_6__["HeaderModule"]
        ],
        exports: [_swapi_list_component__WEBPACK_IMPORTED_MODULE_5__["SwapiListComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        entryComponents: [_swapi_list_component__WEBPACK_IMPORTED_MODULE_5__["SwapiListComponent"]]
    })
], SwapiListModule);



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth-guard */ "./src/app/auth-guard.ts");




let HeaderComponent = class HeaderComponent {
    constructor(router, authGuard) {
        this.router = router;
        this.authGuard = authGuard;
    }
    logout() {
        this.authGuard.logout();
        this.router.navigate(['/login']);
    }
};
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]])
], HeaderComponent);



/***/ }),

/***/ "./src/app/header/header.module.ts":
/*!*****************************************!*\
  !*** ./src/app/header/header.module.ts ***!
  \*****************************************/
/*! exports provided: HeaderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderModule", function() { return HeaderModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./header.component */ "./src/app/header/header.component.ts");






let HeaderModule = class HeaderModule {
};
HeaderModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
        ],
        exports: [_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]],
        entryComponents: [_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"]]
    })
], HeaderModule);



/***/ }),

/***/ "./src/app/login/login-form/login-form.ts":
/*!************************************************!*\
  !*** ./src/app/login/login-form/login-form.ts ***!
  \************************************************/
/*! exports provided: LoginForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginForm", function() { return LoginForm; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");



let LoginForm = class LoginForm {
    constructor(formBuilder) {
        this.formBuilder = formBuilder;
    }
    createForm() {
        return this.formBuilder.group({
            account: this.formBuilder.control('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ]),
            password: this.formBuilder.control('', [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])
        });
    }
};
LoginForm = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], LoginForm);



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1.header {\n    text-decoration: underline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDBCQUEwQjtBQUM5QiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMS5oZWFkZXIge1xuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth-guard */ "./src/app/auth-guard.ts");
/* harmony import */ var _login_form_login_form__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login-form/login-form */ "./src/app/login/login-form/login-form.ts");





let LoginComponent = class LoginComponent {
    constructor(router, formLogin, authGuard) {
        this.router = router;
        this.formLogin = formLogin;
        this.authGuard = authGuard;
        this.form = this.formLogin.createForm();
    }
    getDataForm() {
        return Object.assign(this.form.getRawValue());
    }
    login() {
        const data = this.getDataForm();
        this.account = data.account;
        this.password = data.password;
        if (this.account === 'swapi' && this.password === 'swapi123') {
            this.authGuard.validLogin();
            this.router.navigate(['/dashboard']);
        }
    }
};
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/index.js!./src/app/login/login.component.html"),
        styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
        _login_form_login_form__WEBPACK_IMPORTED_MODULE_4__["LoginForm"],
        _auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]])
], LoginComponent);



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: true,
    END_POINT: 'https://swapi.co/api/films/'
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/bsena/pessoais/poc-swapi/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map