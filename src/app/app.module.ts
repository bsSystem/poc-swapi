import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { AuthGuard } from './auth-guard';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { MzInputModule, MzValidationModule } from 'ngx-materialize';

import { DashboardService } from './dashboard/service/dashboard.service';
import { DashboardRestService } from './dashboard/service/dashboard-rest.service';

import { LoginForm } from './login/login-form/login-form';

import { LoginComponent } from './login/login.component';
import { HeaderModule } from './header/header.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SwapiListModule } from './dashboard/swapi-list/swapi-list.module';
import { SwapiDetailModule } from './dashboard/swapi-detail/swapi-detail.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    FormsModule,
    MzInputModule,
    RouterModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MzValidationModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HeaderModule,
    DashboardModule,
    SwapiListModule,
    SwapiDetailModule
  ],
  providers: [
    LoginForm,
    AuthGuard,
    DashboardService,
    DashboardRestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
