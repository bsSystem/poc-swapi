import { Router } from '@angular/router';
import { Component } from '@angular/core';

import { AuthGuard } from '../auth-guard';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(
    private router: Router,
    private authGuard: AuthGuard) { }

  public logout() {
    this.authGuard.logout();
    this.router.navigate(['/login']);
  }

}
