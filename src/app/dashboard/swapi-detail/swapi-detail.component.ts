import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { SwapiModel } from '../model/swapi.model';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-swapi-detail',
  templateUrl: './swapi-detail.component.html',
  styleUrls: ['./swapi-detail.component.css']
})
export class SwapiDetailComponent implements OnInit {

  public idFilm: string;
  public swapi: SwapiModel;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getFilmById();
  }

  private getFilmById(): void {
    this.idFilm = this.activatedRoute.snapshot.paramMap.get('id');
    this.dashboardService.getFilmById(this.idFilm)
      .subscribe(res => {
        this.swapi = res;
      });
  }

  public back(): void {
    this.location.back();
  }

}
