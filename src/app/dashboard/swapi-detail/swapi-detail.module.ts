import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SwapiDetailComponent } from './swapi-detail.component';
import { HeaderModule } from '../../header/header.module';

@NgModule({
  declarations: [ SwapiDetailComponent ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HeaderModule
  ],
  exports: [ SwapiDetailComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents: [ SwapiDetailComponent ]
})
export class SwapiDetailModule { }
