import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { SwapiDetailComponent } from './swapi-detail.component';
import { DashboardRestService } from '../service/dashboard-rest.service';
import { HeaderModule } from 'src/app/header/header.module';

describe('SwapiDetailComponent', () => {
  let component: SwapiDetailComponent;
  let fixture: ComponentFixture<SwapiDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientModule, HeaderModule ],
      declarations: [ SwapiDetailComponent ],
      providers: [ DashboardRestService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapiDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
