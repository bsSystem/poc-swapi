import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { SwapiListComponent } from './swapi-list.component';

import { HeaderModule } from '../../header/header.module';
import { DashboardRestService } from '../service/dashboard-rest.service';

describe('SwapiListComponent', () => {
  let component: SwapiListComponent;
  let fixture: ComponentFixture<SwapiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientModule, HeaderModule ],
      declarations: [ SwapiListComponent ],
      providers: [ DashboardRestService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwapiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
