import { Component, OnInit } from '@angular/core';

import { SwapiModel } from '../model/swapi.model';
import { FilmsModel } from './../model/films.model';

import { DashboardService } from './../service/dashboard.service';

@Component({
  selector: 'app-swapi-list',
  templateUrl: './swapi-list.component.html',
  styleUrls: ['./swapi-list.component.css']
})
export class SwapiListComponent implements OnInit {
  public films: SwapiModel;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.loadData();
  }

  public loadData(): void {
    this.dashboardService.getAllFilms()
      .subscribe(res => {
        this.films = res;
      });
  }

}
