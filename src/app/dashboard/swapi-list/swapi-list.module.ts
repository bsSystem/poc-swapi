import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SwapiListComponent } from './swapi-list.component';
import { HeaderModule } from '../../header/header.module';


@NgModule({
  declarations: [ SwapiListComponent ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HeaderModule
  ],
  exports: [ SwapiListComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents: [ SwapiListComponent ]
})
export class SwapiListModule { }
