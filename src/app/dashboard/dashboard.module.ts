import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { HeaderModule } from '../header/header.module';
import { SwapiListModule } from './swapi-list/swapi-list.module';
import { SwapiDetailModule } from './swapi-detail/swapi-detail.module';

@NgModule({
  declarations: [ DashboardComponent ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    HeaderModule,
    SwapiListModule,
    SwapiDetailModule
  ],
  exports: [ DashboardComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  entryComponents: [ DashboardComponent ]
})
export class DashboardModule { }
