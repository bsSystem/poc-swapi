import { FilmsModel } from './films.model';

export class SwapiModel {
  constructor(
    public count?: string,
    public next?: string,
    public previous?: string,
    public results?: FilmsModel[]) { }

}
