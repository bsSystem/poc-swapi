import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { environment } from 'src/environments/environment.prod';

import { SwapiModel } from '../model/swapi.model';

@Injectable()
export class DashboardRestService {

  constructor(private http: HttpClient) { }

  public getAllFilms(): Observable<SwapiModel> {
    return of({})
    .pipe(() => this.http.get<SwapiModel>(`${environment.END_POINT}`));
  }

  public getFilmById(id: string): Observable<SwapiModel> {
    return of({})
    .pipe(() => this.http.get<SwapiModel>(`${environment.END_POINT}${id}/`));
  }
}
