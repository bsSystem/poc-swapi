import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { DashboardService } from '../dashboard.service';
import { DashboardRestService } from '../dashboard-rest.service';

describe('DashboardService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [ DashboardRestService ]
    })
    .compileComponents();
  }));

  it('should be created', () => {
    const service: DashboardService = TestBed.get(DashboardService);
    expect(service).toBeTruthy();
  });
});
