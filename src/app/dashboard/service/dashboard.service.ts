import { Injectable } from '@angular/core';

import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { SwapiModel } from '../model/swapi.model';

import { DashboardRestService } from './dashboard-rest.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private dashboardRestService: DashboardRestService) { }


  public getAllFilms(): Observable<SwapiModel> {
    return this.dashboardRestService.getAllFilms()
      .pipe(catchError(error => {
        console.error({ error });
        return of();
      }));
  }

  public getFilmById(id: string): Observable<SwapiModel> {
    return this.dashboardRestService.getFilmById(id)
      .pipe(catchError(error => {
        console.error({ error });
        return of();
      }));
  }

}
