# LOGIN
swapi : swapi123

# Objetivo
Criar uma aplicação que contenha​: [OK]

# Página de login:

Única página disponível se não estiver logado; [OK]
Criar um usuário básico para acesso: ACCOUNT: swapi | PASSWORD: swapi123 [OK]
Uma página de lista de filmes Star Wars: [OK]
Uma página com os detalhes de um filme específico: [OK]

4 Regras:

Utilizar Angular 4+ para o desenvolvimento; [OK]
Realização de teste unitário [OK]
Usar um sistema de controle de versão para entregar o teste (Github, Bitbucket, ...) [OK]
READ-me [OK]

O uso de libs é livre. [OK]

#API

https://swapi.co [OK]

GET .../films/ [OK]
lista de todos os filmes

GET .../films/:id/ [OK]
detalhes de um filme

# PoC-SWAPI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
